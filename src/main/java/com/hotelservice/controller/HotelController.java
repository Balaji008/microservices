package com.hotelservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotelservice.dto.HotelDto;
import com.hotelservice.service.HotelService;

@RestController
@RequestMapping("/hotels")
public class HotelController {

	@Autowired
	private HotelService hotelService;

	@PostMapping("/add-hotel")
	public ResponseEntity<?> addHotel(@RequestBody HotelDto hotelDto) {
		HotelDto hotel = this.hotelService.addHotel(hotelDto);

		return new ResponseEntity<HotelDto>(hotel, HttpStatus.CREATED);
	}

	@GetMapping("/all-hotels")
	public ResponseEntity<?> getAllHotels() {
		List<HotelDto> allHotels = this.hotelService.getAllHotels();

		return new ResponseEntity<List<HotelDto>>(allHotels, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getHotelById(@PathVariable Long id) {
		HotelDto hotel = this.hotelService.getHotelById(id);

		return new ResponseEntity<HotelDto>(hotel, HttpStatus.OK);
	}
}
