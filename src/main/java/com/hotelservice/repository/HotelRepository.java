package com.hotelservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotelservice.entity.HotelEntity;

public interface HotelRepository extends JpaRepository<HotelEntity, Long> {

}
