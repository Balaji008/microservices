package com.hotelservice.service;

import java.util.List;

import com.hotelservice.dto.HotelDto;

public interface HotelService {

	HotelDto addHotel(HotelDto hotelDto);

	List<HotelDto> getAllHotels();

	HotelDto getHotelById(Long id);
}
