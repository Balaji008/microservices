package com.hotelservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelservice.dto.HotelDto;
import com.hotelservice.entity.HotelEntity;
import com.hotelservice.exception.ResourceNotFoundException;
import com.hotelservice.repository.HotelRepository;
import com.hotelservice.service.HotelService;

@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	private HotelRepository repository;

	@Override
	public HotelDto addHotel(HotelDto hotelDto) {
		HotelEntity entity = new HotelEntity();
		entity.setName(hotelDto.getName());
		entity.setLocation(hotelDto.getLocation());
		this.repository.save(entity);
		hotelDto.setId(entity.getId());
		return hotelDto;
	}

	@Override
	public List<HotelDto> getAllHotels() {
		List<HotelEntity> hotels = this.repository.findAll();
		List<HotelDto> hotelDtos = new ArrayList<>();

		hotels.forEach(h -> {
			HotelDto hotelDto = new HotelDto();
			hotelDto.setId(h.getId());
			hotelDto.setName(h.getName());
			hotelDto.setLocation(h.getLocation());
			hotelDtos.add(hotelDto);
		});
		return hotelDtos;
	}

	@Override
	public HotelDto getHotelById(Long id) {
		HotelEntity entity = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Hotel not found", "Hotel not found"));
		HotelDto hotelDto = new HotelDto();
		hotelDto.setId(entity.getId());
		hotelDto.setName(entity.getName());
		hotelDto.setLocation(entity.getLocation());
		return hotelDto;
	}

}
